#!/bin/sh

# Run sitemap generator if mtimes have changed

cd "$(dirname "$0")"

echo "Running sitemapper..."

../../scripts/sitemapper.py && (
	unset GIT_DIR
	cd ../../docs
#	pwd && export 
	git add sitemap.xml
)

echo "Finished!"