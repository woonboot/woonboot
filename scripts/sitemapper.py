#!/usr/bin/env python

import os
import datetime
import sys
from glob import glob

dir_path = os.path.dirname(os.path.realpath(__file__))
docroot = dir_path + '/../docs'
dest_path = docroot + '/sitemap.xml'

template = """<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
{urls}</urlset>
"""

def get_files():
	rst_files = glob(docroot + '/*.rst')
	files = []
	for path in rst_files:
		if not os.path.getsize(path):
			continue
		filename = os.path.basename(path)[:-4]
		mtime = os.path.getmtime(path)
		mtime = datetime.datetime.utcfromtimestamp(mtime).strftime('%Y-%m-%dT%H:%M:%S+00:00')
		files.append((filename, mtime))
	return files


def files_to_xml(files):
	allxml = ""
	for file, mtime in files:
		if file == 'index':
			file = ''
			prio = '1.00'
		else:
			file += '.html'
			prio = '0.80'
		allxml += """<url>
  <loc>https://woonboot.readthedocs.io/{file}</loc>
  <lastmod>{mtime}</lastmod>
  <priority>{prio}</priority>
</url>
""".format(**locals())
	return allxml

if __name__ == '__main__':
	files = get_files()
	urlxml = files_to_xml(files)

	origfile = open(dest_path).read()
	newfile = template.format(urls=urlxml)

	if origfile != newfile:
		print("Sitemap changed, writing new version..")
		with open(dest_path, 'w') as fh:
			fh.write(template.format(urls=urlxml))
		sys.exit(0)
	else:
		print("No changes in sitemap.xml required.")
		sys.exit(1)

