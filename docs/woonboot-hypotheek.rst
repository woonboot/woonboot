Woonboot hypotheek
==================
Je hebt de woonboot van je dromen gevonden en je hoeft hem alleen nog maar te betalen. Dan is het handig als je ergens een hypotheek kunt krijgen. En daar komt nog wat bij kijken, aangezien banken minder enthousiast zijn over zo'n drijvende risicopost. Lees hier waar je op moet letten.

.. image:: _static/woonboot-hypotheek.jpg
   :alt: Woonboot hypotheek

Aanbieders
------------------------------
In Nederland waren voorheen drie aanbieders, maar dat is er helaas nog maar één. De Friesland Bank is overgenomen door de Rabobank en de ING is ermee gestopt want te ingewikkeld. Je bent dus aangewezen op de Rabobank. Heel misschien zou je ook nog bij een Belgische bank een hypotheek kunnen krijgen.

Nu zou de waarde van woonboten kelderen als er geen hypotheken meer mogelijk waren. Dat is ook niet in het voordeel van de Rabobank, dus die heeft bezworen dat ze hun woonboot hypotheek handhaven. Dat is op zich redelijk nieuws. Het nadeel is dat er nu dus effectief één aanbieder is: een monopolist. 

Aandachtspunten
----------------------------------
De bank zal een opslag rekenen op het rente tarief, omdat het risico van een woonboot moeilijker te berekenen is en dus hoger uitvalt. Dit kan oplopen tot 0,8%.

Een bank vereist een goede verzekering tegen brandschade en dergelijke, maar daarnaast ook voor zinkschade. Voor woonboten zijn diverse verzekeringen beschikbaar, die kosten zo'n 500 tot 1000 euro per jaar. Deze geven wat specifieke eisen, zoals vijfjaarlijkse controle op de werf (voor metalen casco's) en ramen moeten tenminste 15 centimeter boven de waterlijn vallen. 

Een bank vereist een taxatierapport van een erkend taxateur. Dat kost zo'n 500 euro.

Een woonboot hypotheek moet worden ingeschreven in het scheepsregister van het kadaster. Dat is iets duurder dan bij een gewoon huis, dus notariskosten en dergelijke vallen iets hoger uit dan bij een woonhuis. Daar staat tegenover dat een woonboot een roerende zaak is, en er dus geen overdrachtsbelasting (nu 2%) wordt gerekend. 

Van de bank mag je officieel geen BnB beginnen op je boot. Verhuren tijdens je vakantie (en al helemaal niet professioneel) zit er dus niet in. Al zijn er wel mensen die het stiekem doen. 

Geen woonboot hypotheek mogelijk?
---------------------------------
Als de bank om wat voor reden niet mee wil werken, zou je een persoonlijke lening kunnen overwegen. Daarvan ligt het rentepercentage wel een stuk hoger en de looptijd is doorgaans korter. Wel kun je de rente aftrekken van je belasting, mits je een vaste ligplaats hebt. 