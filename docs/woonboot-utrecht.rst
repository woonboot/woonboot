Woonboot Utrecht
================
Net als in Amsterdam is de huizenmarkt in Utrecht behoorlijk oververhit. Maar, als je geschikt bent als bootbewoner zou je een woonboot in Utrecht kunnen overwegen. In Utrecht zijn namelijk driehonderd woonboten en ligplaatsen! Op sommige plaatsen in het centrum kun je voor een relatief laag bedrag een woonark of woonschip bemachtigen. Waar moet je op letten? Hieronder enkele tips. 

.. image:: _static/woonboot-utrecht.jpg
   :alt: Woonboot Utrecht

Huren in Utrecht
-------------------------
Er zijn af en toe woonboten te huur in Utrecht. Maar let op: als huurder van een boot heb je niet zoveel rechten als op de vaste wal. Zo heb je geen huurbescherming, dus de eigenaar kan je er aan het einde van het huurcontract gewoon uitzetten. Daarnaast moet je gemeentebelasting betalen die normaal gesproken door de eigenaar van een pand wordt betaald. En let op wat je afspreekt over onderhoud: hoe snel kan de eigenaar een kapotte rioolpomp komen repareren? 

Voor het vinden van een verhuurbare woonboot kun je het beste Marktplaats in de gaten houden of briefjes bij mensen in de brievenbus doen. Wil je eens uitproberen wat het is om op een woonboot te wonen, dan zijn er diverse boten te huren voor een nachtje op AirBNB. 

Kopen in Utrecht
-------------------------
Veel, maar niet alle te koop aangeboden woonboten komen op Funda. Daarom kun je ook het beste Marktplaats in de gaten houden, als ook de sites van de twee gespecialiseerde woonboot makelaars in Utrecht. Dat zijn Niek van der Sluis en Peter van Reenen. Ook kun je eens aanbellen om te informeren of er soms buren met verhuisplannen zijn. 

De meeste boten in Utrecht liggen in de Vecht, het Merwedekanaal, de Veilinghaven en de Burt Bacharachstraat (Leidsche Rijn). 

Check altijd het bestemmingsplan van de gemeente: wat staat er de komende jaren te gebeuren? Is er in de gemeenteraad al voorgesteld om de boten te verplaatsen? Worden er grote flats gebouwd? Verdwijnt gratis parkeren? Dat kun je allemaal gebruiken in je prijsonderhandeling. 

Voor het bezichtigen van een woonboot is het raadzaam om een ervaringsdeskundige mee te nemen, zoals een bootbewoner of een gespecialiseerde makelaar. Die weet feilloos de zwakke plekken aan te wijzen (lekkages, pomp, betonrot).


Woonboot vereniging Utrecht
---------------------------
Utrecht heeft de meest actieve belangenvereniging voor bootbewoners genaamd `SUWO <http://www.suwo.nl>`_. Voor een paar tientjes per jaar kun je lid worden. Zij houden je op de hoogte over relevante ontwikkelingen. En als het nodig is, springen ze op de bres om jouw belangen te beschermen. Bijvoorbeeld bij het verhogen van liggelden of het gedwongen verplaatsen van boten. Ook organiseren ze informatieve bijeenkomsten over bijvoorbeeld isolatie en duurzame bouwmaterialen. 



