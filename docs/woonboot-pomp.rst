Woonboot pomp
=================

In Nederland mogen woonboten niet op het oppervlaktewater lozen en moeten dus een aansluiting hebben op het riool. Omdat boten onder straatniveau liggen, moet het afvalwater worden opgepompt. Dit gebeurt met een fecaliën- of rioolpomp. Om het allemaal weg te krijgen wordt het spul eerst vermaald, dit kan een beste herrie geven. Maar na een weekje hoor je het niet meer. 

.. image:: _static/woonboot-pomp.png
   :alt: Woonboot rioolpomp

Bekende merken zijn Jung en KSB. 

Pomp verstopt
-------------

De pomp slaat even aan maar houdt er direct mee op? 
Of de pomp controller slaat alarm? 

Check of de vermaler heet is. Is dat het geval, dan heb je zeer waarschijnlijk last van een verstopping. Soms helpt het om wat soda en heet water in het reservoir te mikken en een uurtje te wachten. Maar meestal moet je de boel uit elkaar schroeven om het verstoppende object/tampon/wattenstaafje/knaagdier eruit te peuteren. Geen leuk werkje, maar je hebt na afloop wel een stoer verhaal. Ook nuttig: een oude waterstofzuiger om het reservoir tijdelijk leeg te maken.

Pomp werkt niet
----------------

Klikt de pompt maar lijkt hij verder niet aan te slaan?

Een veelvoorkomend eufel is een kapotte condensator, die zal moeten worden vervangen.

Een andere mogelijkheid is dat er lucht in het pompsysteem zit. Wacht een uurtje, en als dat niet helpt, schroef alles uit elkaar. 

Pomp te hoog
------------

Is je afvoer lager dan de pomp-ingang, bijvoorbeeld bij een douche op de onderverdieping, dan is het het best om je douche te verhogen. Is dat niet mogelijk, dan kun je een hulp-pomp (zonder vermaler) installeren die je douchewater enkele centimeters omhoog pompt.
