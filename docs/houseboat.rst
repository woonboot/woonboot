Dutch Houseboat
===============

When visiting Holland, it is highly recommended to stay on a houseboat in one of the picturesque Dutch canals. The water ways of Amsterdam and Utrecht are the most popular.

Short stay or holiday
---------------------
Numerous houseboats are available for rent. In fact, most of the boats in Amsterdam can be booked through sites such as AirBNB. The rental price is slightly higher than for regular apartments, but you will surely enjoy life on the water. 

Note that most boats are, due to their proximity to water, are not suited for small children. Also, check whether you will rent a complete boat or whether you will share it. The sewage system can be quite noisy so you would rather not share that. 

Do not put anything other than normal toilet paper in the toilet. Houseboats have a delicate sewage system that will easily get jammed if used for anything but normal sewage. And that would be a lousy inconvenience during your holiday.

Living on a houseboat
---------------------
Renting a houseboat for extended periods is also possible. Note that the general Dutch protection of renters does not apply to renters of houseboats, so the owner could expel you once the rental contract expires. 
