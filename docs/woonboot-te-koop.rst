Woonboot te koop
================
In Nederland liggen zo'n 12000 woonboten waarvan er per jaar een kleine honderd worden verkocht. Met een beetje geluk - en geduld - vind je dus jouw droomboot! Hieronder een berg nuttige tips voor de aankoop van jouw woonboot.

.. image:: _static/woonboot-te-koop.jpg
   :alt: Woonboot te koop

Ben jij geschikt als bootbewoner?
---------------------------------
Woonboot-bewoners zijn meestal ondernemende en handige types. Dat komt omdat het wonen op een woonboot iets meer risico meebrengt, maar ook heel veel oplevert. Je hebt (iets) meer onderhoud, en je moet je er prettig bij voelen dat je je in een on-ontgonnen gebied van de wet begeeft. De meeste regels (huurbescherming, welstandscommissies, onroerende zaak belasting) zijn namelijk niet van toepassing op boten. Maar daar krijg je, voor een relatief laag bedrag, een vrijstaand huis (vaak met tuin) midden in de stad voor terug.  

Woonboot of ligplaats
---------------------
Let op! In Nederland komen er praktisch geen nieuwe ligplaatsen bij. Omdat wel veel mensen hun oude boot laten vervangen door een nieuwe, zijn er meer boten dan ligplaatsen. Een woonboot zonder ligplaats is dus weinig waard. De juist volgorde is dus: vind eerst een ligplaats en dan pas een woonboot. 

Waarde ligplaats
----------------
De waarde van een woonboot wordt grotendeels bepaald door de ligplaats. In Utrecht en Amsterdam wordt in 2017 zo'n twee tot drie ton gevraagd voor een lege ligplaats. Een ligplaats is echter (vrijwel) nooit eigendom; men verkoopt het recht op het huurcontract. Dit huurcontract is gesloten met de eigenaar van het water, dat is meestal de gemeente of Rijkswaterstaat. Deze huurcontracten zijn in het algemeen overdraagbaar. 

Waarde woonboot 
---------------
De waarde van boten zelf lopen uiteen van 1500 euro (een ex-prostitutieboot) tot 750.000 euro (een drijvend paleis). Belangrijk is de staat van onderhoud van de buitenkant. Omdat boten bewegen en door weerkaatsing van het water meer zonlicht te verduren krijgen, heeft de buitenkant meer last van slijtage dan een huis aan de wal. Daarnaast heeft een woonboot een aantal bewerkelijke onderdelen zoals een rioolpomp en walbevestiging. 

Woonark of woonschip
--------------------
Er zijn twee soorten woonboten: a) drijvende huizen met een betonnen bak en b) voormalige schepen met een metalen casco. Een schip heeft meer karakter maar vereist ook meer onderhoud, zo verplicht de verzekering om de stalen bodem eens in de vijf jaar te laten inspecteren op een werf. Dat betekent dus af en toe een weekje verhuizen naar het vaste land! 

Een betonnen bak vergt nagenoeg geen onderhoud. De meeste betonnen casco's die halverwege de vorige eeuw zijn gebouwd, gaan nog steeds mee. Een veilige keuze voor de luie bootbewoner dus.

Woonboot risico's
-----------------
Wonen op een woonboot geeft een hoop voordelen maar ook enkele risico's. Dit komt voornamelijk omdat woonboten in de wet niet onder "onroerend goed" vallen. Daarnaast zijn ligplaatsen meestal geen eigendom. En de meeste huurcontracten kennen een opzegtermijn van drie maanden. Het gebeurt wel eens dat woonboten onder dwang worden verplaatst naar een minder "pittoreske" locatie. Wel lijken gemeenten er meer rekening mee te houden dat men forse hypotheken afsluit voor een ligplaats. Als je op een vaart ligt met veel andere woonboten, is de kans dat je in de toekomst zal worden verplaatst een stuk kleiner. 

Daarnaast heb je natuurlijk het risico op zinken. Ook het risico op woninginbraak lijkt ietsje groter, vooral in de zomer en als er ijs ligt. 

Nieuwe woonboot laten bouwen
----------------------------
Bij diverse arkenbouwers kun je een boot op maat bestellen. Een nieuwe woonark met betonnen bak en houten opbouw met 2 verdiepingen kost 150.000 tot 300.000 euro. Groot voordeel: woonboten zijn aan weinig regels gebonden, dus je kunt het ontwerp zo gek maken als je zelf wil. Enkel de buitenmaten zijn (per gemeente) gelimiteerd. 