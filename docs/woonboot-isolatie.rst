Woonboot isolatie
=================

Woonboten zijn eigenlijk vrijstaande huizen en hebben dus aan alle kanten warmteverlies. Als je een oudere woonboot hebt, is het interessant om te investeren in isolatie. Dit verdien je meestal binnen een paar jaar weer terug.

.. image:: _static/woonboot-isolatie.jpg
   :alt: Woonboot isolatie


Om te beginnen kun je voor een paar tientjes een warmtecamera huren. Daarmee zie je in kleuren waar je warmte het huis uit rent. Roder is warmer, en dat is bijvoorbeeld bij ramen, ventilatieroosters en je dak. 

Waterleidingen
--------------
In de winter is het allerbelangrijkste dat je leidingen niet bevriezen en stukgaan. Aangezien waterleidingen door de lucht naar de kade gaan, hebben ze isolatie nodig. Gewone kunststof isolatiehulzen van de bouwmarkt volstaan, maar leg er wel een electrisch warmtelint in. De meeste warmtelinten gaan pas werken vanaf een bepaalde temperatuur (onder 2 graden C) en kun je dus het hele jaar in het stopcontact laten zitten. 

Warmteterugwinning
------------------
Om geen warmte te verliezen maar toch te ventileren is een mechanische ventilatie met warmteterugwinning geschikt. Deze haalt de energie uit de lucht die naar buiten stroomt en warmt daarmee de binnenkomende lucht op. 

Vloerisolatie
-------------
Zeker voor een stalen romp kan er veel energie via het water weglekken. Sommige mensen storten een laag beton in de romp, maar dat is niet aan te raden: inspectie is niet meer mogelijk en toekomstige roest is lastig te repareren. 

Veel nieuwe woonarken worden afgeleverd met ofwel vloerverwarming, ofwel een laag polystyreen (piepschuim) en daar bovenop cementvezels. 
