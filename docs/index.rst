.. Woonboot documentation master file, created by
   sphinx-quickstart on Thu Nov 16 22:13:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Woonboot, van A tot Z
================

In Nederland wonen zo'n twintigduizend mensen op een boot. Bootbewoners zijn vaak avontuurlijk en ondernemend. En pakken de zaken net iets anders aan. Dat moet ook wel, want een woonboot brengt bijzondere rechten en plichten mee. 

Al lang bootbewoner of overweeg je op het water te gaan wonen? Hieronder vind je handige informatie over alles wat er bij komt kijken. Van rioolpomp tot liggeld, van isolatie tot verzekering. 

.. image:: _static/woonboot.jpg
   :alt: Woonboot (© Henk-Jan van der Klis)

.. toctree::
   :maxdepth: 2
   :caption: Alle onderwerpen

   woonboot-te-koop
   woonboot-amsterdam
   ligplaats-woonboot
   woonboot-utrecht
   woonboot-groningen
   woonboot-makelaar
   woonboot-taxatie
   woonboot-verzekering
   woonboot-hypotheek
   woonboot-onderhoud
   woonboot-isolatie
   woonboot-pomp
   houseboat

