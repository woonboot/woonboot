# dev howto

```
sudo apt install python3-sphinx
sudo pip3 install sphinx-autobuild
```


Run watcher:
```
cd docs
sphinx-autobuild . _build/html/
```

# Todo

1. Top 10 keywords verzamelen
1. Pagina per keyword maken
1. Text vullen in sphinx
1. Promoten her en der

# keywords

woonboot
woonboot te koop
woonboten
woonboot amsterdam
woonboot huren
houseboat
woonboot kopen
ligplaats woonboot
funda woonboot
woonboot te huur
woonboot te koop amsterdam
woonboot groningen
woonboot utrecht
we hebben een woonboot
woonboot huren amsterdam


uit text analyse:

beste woonboot
woonark
ligtplaats
schip
betonnen bak
casco
stalen bak
gemeente
amsterdam
terras
beton 
bouwen
hypotheek
woonschip
oplossing
vergunning
onderhoud
isolatie
pomp
verzekering
makelaar
taxatie

Beste 
10

woonboot-te-koop
woonboot-amsterdam
woonboot-huren
houseboat
ligplaats-woonboot
woonboot-utrecht
woonboot-groningen

woonboot-makelaar
woonboot-taxatie
woonboot-verzekering
woonboot-hypotheek
woonboot-onderhoud
woonboot-isolatie
woonboot-pomp